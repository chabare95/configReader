package configReader

import (
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"
)

// Config is the basic 'struct' for the configFile
type Config struct {
	Name string
}

// Read reads the config file with the default delimiter (=)
func (c Config) Read() map[string]string {
	return c.ReadDelimiter("config", "=")
}

// ReadFilename reads the named file with the default delimiter (=)
func (c Config) ReadFilename(filename string) map[string]string {
	return c.ReadDelimiter(filename, "=")
}

// ReadDelimiter reads the config with a specific delimiter
func (c Config) ReadDelimiter(filename, delim string) map[string]string {
	lines := getLines(open(filename))

	return getMap(lines, delim)
}

func getMap(lines []string, delim string) map[string]string {
	config := make(map[string]string)

	keys, values := getKeysAndValues(lines, delim)

	for i := range keys {
		config[keys[i]] = values[i]
	}

	return config
}

func getKeysAndValues(lines []string, delim string) ([]string, []string) {
	keys := make([]string, len(lines))
	values := make([]string, len(lines))
	regex, err := regexp.Compile("([^" + delim + " \t\r\n]+) *" + delim + " *([^" + delim + "\t\r\n]+)")
	if err != nil {
		log.Println(err)
	}
	for i, line := range lines {
		if !regex.MatchString(line) {
			fmt.Println("No match")
			continue
		}

		// It definitly finds something (indirect guard by MatchString)
		keys[i] = regex.FindStringSubmatch(line)[1]
		values[i] = regex.FindStringSubmatch(line)[2]
	}

	return keys, values
}

func getKeys(lines []string, delim string) []string {
	keys := make([]string, len(lines))
	regex, err := regexp.Compile("([^" + delim + " \t\r\n]+) *" + delim + " *[^" + delim + "\t\r\n]+")
	if err != nil {
		log.Println(err)
	}
	for i, line := range lines {
		if !regex.MatchString(line) {
			fmt.Println("No match")
			continue
		}

		// It definitly finds something (indirect guard by MatchString)
		keys[i] = regex.FindStringSubmatch(line)[1]
	}

	return keys
}

func getValues(lines []string, delim string) []string {
	values := make([]string, len(lines))
	regex, _ := regexp.Compile("[^" + delim + " \t\r\n]+ *" + delim + " *([^" + delim + "\t\r\n]+)")

	for i, line := range lines {
		if !regex.MatchString(line) {
			fmt.Println("No match")
			continue
		}

		// It definitly finds something (indirect guard by MatchString)
		values[i] = regex.FindStringSubmatch(line)[1]
	}

	return values
}

func getLines(file *os.File) []string {
	data := make([]byte, 4096)
	file.Read(data)
	lines := strings.Split(string(data), "\n")

	// Check for empty last line
	if regexp.MustCompile("[^ \t\n]").MatchString(lines[len(lines)-1]) {
		lines = lines[:len(lines)-1]
	}

	file.Close()

	return lines[:]
}

func open(name string) *os.File {
	f, err := os.Open(name)

	if err != nil {
		log.Fatal(err)
	}

	return f
}
